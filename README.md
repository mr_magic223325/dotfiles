# My Personal Configs
#### Feel Free To Steal Stuff (I Did The Same)

### [Arch](../../tree/arch)
#### Arch Configs

### [Gentoo](../../tree/gentoo)
#### Gentoo Configs

Content
==
- Alacritty
- Bspwm
- FLAC Converter Script
- Fonts
- Luke Smith emoji script (modified for Rofi)
- Lock script
- Neovim
- Picom
- Polybar
- Ranger
- Rofi
- Sxhkd
- TIFF Converter Script
- Timer Script
- Wallpapers changing based on the time
- Weather report downloader

Dependencies
==
! Might Be Outdated !
- Ripgrep (Telescope (NeoVim) Uses That)
- Zsh (Use Whatever Shell You Want)
- Iosevka Custom (Use My Version Or Just Change The Font In [alacritty.yml](alacritty/alacritty.yml), [sxhkdrc](sxhkd/sxhkdrc) And [config.rasi](rofi/config.rasi))
- Some Nerd Font (Used By [Polybar](polybar/bspwm_cfg.ini))
- Dunst (Fancy Notifications)
- KDE Polkit (Other Polkit Also Works Just Edit [bspwmrc](bspwm/bspwmrc))
- Mpd-mpris (Only If You Plan On Using MPD As Your Music Player And My [Polybar](polybar/bspwm_modules.ini))
- Nitrogen (Wallpaper Changer Used In [bg.sh](bspwm/scripts/bg.sh))
- Scrot (Screenshot Tool Used In [screenshot.sh](bspwm/scripts/screenshot.sh))
- Zathura (Document Viewer Used In [nvim-latex.sh](scripts/nvim-latex.sh))
- Ffmpeg (For [flac.sh](scripts/flac.sh) And [tiff.sh](scripts/tiff.sh))
- Betterlockscreen (Fancy Lockscreen Used By [lock.sh](scripts/lock.sh))
- XeLaTeX (Only If You Plan On using LaTeX And [nvim-latex.sh](scripts/nvim-latex.sh))
